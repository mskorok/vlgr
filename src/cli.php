<?php

namespace App;

use App\Data\Buyer;
use App\Data\Order;


require_once 'Data/BuyerInterface.php';
require_once 'Data/AbstractOrder.php';
require_once 'Data/Buyer.php';
require_once 'Data/Order.php';
require_once 'ShippingServiceInterface.php';
require_once 'ShippingService.php';

$buyerId = 29664;
$orderId = 16400;

$buyerFileName = __DIR__ . '/../results/' . 'Buyer_' . $buyerId . '.log';

$buyerData = file_get_contents(__DIR__ . '/../mock/buyer.' . $buyerId . '.json');
try {
    $buyerData = json_decode($buyerData, false, 512, JSON_THROW_ON_ERROR);
} catch (\JsonException $e) {
    echo $e->getLine() . 'cli.php/' . $e->getMessage();
    die();
}

if (file_exists($buyerFileName)) {
    $buyer = unserialize(file_get_contents($buyerFileName));
    if (!($buyer instanceof Buyer)) {
        $buyer = new Buyer($buyerData);
    }
} else {
    $buyer = new Buyer($buyerData);
}

try {
    $order = new Order($orderId);
} catch (\JsonException $e) {
    echo $e->getLine() . 'cli.php/' . $e->getMessage();
    die();
}

$service = new ShippingService();

$trackingNumber = $service->ship($order, $buyer);

$order->setTrackingNumber($trackingNumber);

$offset = count($buyer->getOrders());
++$offset;
$buyer->offsetSet($offset, $order);



$r = fopen($buyerFileName, 'wb+');
fwrite($r, serialize($buyer));
fclose($r);

exit('Finish');






