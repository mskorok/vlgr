<?php

namespace App\Data;

class Buyer implements BuyerInterface
{
    /**
     * @var string
     */
    protected string $country_id;

    /**
     * @var string
     */
    protected string $country_code;

    /**
     * @var string
     */
    protected string $country_code3;

    /**
     * @var string
     */
    protected string $shop_username;

    /**
     * @var string
     */
    protected string $email;

    /**
     * @var string
     */
    protected string $phone;

    /**
     * @var string
     */
    protected string $address;

    /**
     * @var array
     */
    protected array $data;

    /**
     * @var array
     */
    private array $orders = [];

    /**
     * @param object $data
     */
    public function __construct(object $data)
    {
        $this->country_id = $data->country_id;
        $this->country_code = $data->country_code;
        $this->country_code3 = $data->country_code3;
        $this->shop_username = $data->shop_username;
        $this->email = $data->email;
        $this->phone = $data->phone;
        $this->address = $data->address;
        $this->data = $data->data;
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset): bool
    {
        return isset($this->orders[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        return $this->orders[$offset] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->orders[] = $value;
        } else {
            $this->orders[$offset] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset): void
    {
        unset($this->orders[$offset]);
    }

    /**
     * @return string
     */
    public function getCountryId(): string
    {
        return $this->country_id;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->country_code;
    }

    /**
     * @return string
     */
    public function getCountryCode3(): string
    {
        return $this->country_code3;
    }

    /**
     * @return string
     */
    public function getShopUsername(): string
    {
        return $this->shop_username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getOrders(): array
    {
        return $this->orders;
    }

    /**
     * @param array $orders
     */
    public function setOrders(array $orders): void
    {
        $this->orders = $orders;
    }
}