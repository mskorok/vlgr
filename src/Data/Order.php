<?php

namespace App\Data;

use JsonException;

class Order extends AbstractOrder
{
    protected string $trackingNumber;

    /**
     * @throws JsonException
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->load($id);
    }

    /**
     * @throws JsonException
     */
    protected function loadOrderData(int $id): array
    {
        $order = __DIR__ . '/../../mock/order.' . (string) $id . '.json';
        $order = file_get_contents($order);
        return json_decode($order, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @return string
     */
    public function getTrackingNumber(): string
    {
        return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     */
    public function setTrackingNumber(string $trackingNumber): void
    {
        $this->trackingNumber = $trackingNumber;
    }
}